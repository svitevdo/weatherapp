import axios from 'axios';
import * as actionType from '../constans/actionType';
const CryptoJS = require('crypto-js');

export function getData(unit) {
    return (dispatch) => {
      dispatch({ type: actionType.FETCH_DATA })

      navigator.geolocation.getCurrentPosition((position) => {
        const url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss';
        const method = 'GET';
        const appId = 'ZNbjJv4q';
        const consumerKey = 'dj0yJmk9VkFHSUk4QzdPc3pOJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PWZj';
        const consumerSecret = '8d72c810e8190f7a26770abe924aca1c0bd5bc71';
        const concat = '&';
        const query = {
            'lat':position.coords.latitude, 
            'lon': position.coords.longitude, 
            'format':'json', 
            'u': unit.toLowerCase()
        };
        const oauth = {
            'oauth_consumer_key': consumerKey,
            'oauth_nonce': Math.random().toString(36).substring(2),
            'oauth_signature_method': 'HMAC-SHA1',
            'oauth_timestamp': parseInt(new Date().getTime() / 1000).toString(),
            'oauth_version': '1.0'
        };

        const merged = Object.assign({}, query, oauth);

        const mergedArr = Object.keys(merged).sort().map((k) => {
            return [`${k}=${encodeURIComponent(merged[k])}`];
        });
        const signatureBaseStr = method
            + concat + encodeURIComponent(url)
            + concat + encodeURIComponent(mergedArr.join(concat));

        const compositeKey = encodeURIComponent(consumerSecret) + concat;
        const hash = CryptoJS.HmacSHA1(signatureBaseStr, compositeKey);
        const signature = hash.toString(CryptoJS.enc.Base64);

        oauth.oauth_signature = signature;
        const authHeader = `OAuth ${Object.keys(oauth).map((k) => {
                return [`${k}="${oauth[k]}"`];
            }).join(',')}`;
        
        axios.get(
            `${url}?lat=${query.lat}&lon=${query.lon}&format=json&u=${unit.toLowerCase()}`, 
            {
                headers: {
                    'Authorization': authHeader,
                    'X-Yahoo-App-Id': appId
            }}).then(
                response => {
                    dispatch({
                        type: actionType.SUCCESS_FETCHING_WEATHER_DATA,
                        payload: response.data.forecasts
                    });
                    dispatch({
                        type: actionType.SUCCESS_FETCHING_LOCATION_DATA,
                        payload: response.data.location
                    });
                },
                err => {
                    dispatch({
                        type: actionType.FAILED_FETCHING_WEATHER_DATA,
                        err
                    });
                    dispatch({
                        type: actionType.FAILED_FETCHING_LOCATION_DATA,
                        err
                    });
                }
            );
        },() => {
            dispatch({
                type: actionType.GEOLOCATION_NOT_AVAILABLE
            });    
        });
    }
  }
