import * as actionType from '../constans/actionType';

export function updateUnit(unit) {
    return {
        type: actionType.UPDATE_UNIT,
        unit
    };
}
export function resetUnit() {
    return {
        type: actionType.RESET_UNIT
    };
}