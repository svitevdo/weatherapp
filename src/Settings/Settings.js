import React, {Component} from 'react';
import {ButtonGroup, Button} from 'react-bootstrap';
import { connect } from 'react-redux';
import {updateUnit, resetUnit} from '../actions/unit';
import './Settings.less';

class Settings extends Component {
    updateUnit = (event) => {
        this.props.updateUnit(event.target.value);
        localStorage.removeItem('weather');
    }

    resetDate = () => {
        this.props.resetUnit();
        localStorage.removeItem('location');
        localStorage.removeItem('weather');
    }

    render() {
        const {chosenUnit} = this.props.state.unit;

        return (
            <div className="settings text-center">
                <div>
                    <p>Units</p>
                    <ButtonGroup>
                        <Button 
                            size="lg" 
                            variant="info" 
                            value='C'
                            disabled={chosenUnit==='C'}
                            onClick={this.updateUnit}>
                            C
                        </Button>
                        <Button 
                            size="lg" 
                            variant="info"
                            value='F'
                            disabled={chosenUnit==='F'}
                            onClick={this.updateUnit}>
                            F
                        </Button>
                    </ButtonGroup>
                </div>
                <div className="system-settings">
                    <p>System settings</p>
                    <Button size="lg" variant="danger" onClick={this.resetDate}><b>Reset Cache</b></Button>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
return {
        state
    }
};

const mapDispatchToProps = (dispatch) => {
return {
    updateUnit: (unit) => dispatch(updateUnit(unit)),
    resetUnit: () => dispatch(resetUnit())    
}
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);