import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import MainPage from './MainPage/MainPage';
import Settings from './Settings/Settings';
import Navigation from './Navigation/Navigation';

function AppRouter() {
    return (
      <Router>
        <Navigation/>
        <Switch>
          <Route path="/" exact component={MainPage} />
          <Route path="/settings" component={Settings} />
          <Redirect from to="/"/>
        </Switch>
      </Router>
    );
}

export default AppRouter;
