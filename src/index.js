import React from 'react';
import ReactDOM from 'react-dom';
import './index.less';
import AppRouter from './AppRouter';

import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from './reducers/index'
import { composeWithDevTools } from 'redux-devtools-extension'

const middlewareEnhancer = applyMiddleware(thunkMiddleware)
const composedEnhancers = composeWithDevTools(
  middlewareEnhancer
)

const store = createStore(rootReducer, {}, composedEnhancers)

if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept('./reducers/index', () => store.replaceReducer(rootReducer))
  }

ReactDOM.render(
  <Provider store={store}>
    <AppRouter />
  </Provider>, document.getElementById('root'));

// serviceWorker.unregister();
