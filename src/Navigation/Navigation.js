import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import './Navigation.less';

class Navigation extends Component {
    render() {
        return (
          <nav className="navigation">
            <Link to="/">
                <p>The best Weather App we ever made</p>
            </Link>
            <Link className="settings-icon" to="/settings"><i className="fas fa-cog"></i></Link>
          </nav>
        )
    }
}

export default Navigation;