import * as actionType from '../constans/actionType';
localStorage.setItem('unit', 'C');

const intitialState = {
    chosenUnit: 'C'
  }

export default function unit(state = intitialState, action) {
  switch (action.type) {
    case actionType.UPDATE_UNIT: 
      localStorage.setItem('unit', action.unit);

      return {
          chosenUnit: action.unit
      }
    case actionType.RESET_UNIT:
      localStorage.setItem('unit', intitialState.chosenUnit);

      return intitialState;
    default:
      return state
  }
}