import * as actionType from '../constans/actionType';

const intitialState = {
    data: []
  }

export default function weatherData(state = intitialState, action) {
  switch (action.type) {
    case actionType.SUCCESS_FETCHING_WEATHER_DATA:
      localStorage.setItem('weather', JSON.stringify(action.payload));
      
      return {
          data: action.payload
      };
    case actionType.FAILED_FETCHING_WEATHER_DATA:
      return {
        error: action.err
      }
    default:
      return state
  }
}