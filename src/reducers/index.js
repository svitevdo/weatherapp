import {combineReducers} from 'redux';

import weatherData from './weatherData';
import locationData from './locationData';
import unit from './unit';

export default combineReducers({
    weatherData,
    locationData,
    unit
  }
);
