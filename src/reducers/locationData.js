import * as actionType from '../constans/actionType';

const intitialState = {
    data: [],
    fetching: true,
    available: true
  }

export default function locationData(state = intitialState, action) {
  switch (action.type) {
    case actionType.FETCH_DATA:
      return {
        ...state,
        fetching: true
      }
    case actionType.GEOLOCATION_NOT_AVAILABLE:{
      return {
        ...state,
        available: false
      }
    }
    case actionType.SUCCESS_FETCHING_LOCATION_DATA:
      localStorage.setItem('location', JSON.stringify(action.payload));
      
      return {
          ...state,
          data: action.payload,
          fetching: false
      };
    case actionType.FAILED_FETCHING_LOCATION_DATA:
      return {
        ...state,
        error: action.err,
        fetching: false
      }
    default:
      return state
  }
}