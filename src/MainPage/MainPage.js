import React, {Component} from 'react';
import { connect } from 'react-redux';
import {getData} from '../actions/weather';
import Page from 'react-page-loading'
import WeatherData from './WeatherData/WeatherData';
import { Waypoint } from 'react-waypoint';
import * as actionType from '../constans/actionType';
import './MainPage.less';

class MainPage extends Component {

    componentDidMount() {
        const location = JSON.parse(localStorage.getItem('location'));
        const weather = JSON.parse(localStorage.getItem('weather'));

        if (!location || !weather){
            this.props.getData(this.props.state.unit.chosenUnit);
        } else {
            this.props.setLocation(location);
            this.props.setWeather(weather);
        }
    }

    render() {
        const weatherArray = this.props.state.weatherData.data
            .map(info => <WeatherData key={info.date} weather={info}/>);
        const location = localStorage.getItem('location');
        const weather = localStorage.getItem('weather');
        const megaTextForNick = 'Nick voz\'my menya or turn on sharing your location';

        if (!this.props.state.locationData.available && (!location || !weather)) {
            return (<div className="text-center"><h1>{megaTextForNick}</h1></div>);
        }

        return this.props.state.locationData.fetching ? <Page loader={'bubble-spin'} color={'#A9A9A9'} size={10}/> :(
            <div className="main-page">
                <p className="location-header">Weather at {this.props.state.locationData.data.city}</p>
                <Waypoint horizontal={true}>
                    <div className="weathers">
                        {weatherArray}
                    </div>
                </Waypoint>
            </div>);
    }
}

const mapStateToProps = (state) => {
return {
    state
}};

const mapDispatchToProps = (dispatch) => {
return {
    getData: (unit) => dispatch(getData(unit)),
    setWeather: (data) => dispatch({type: actionType.SUCCESS_FETCHING_WEATHER_DATA, payload: data}),
    setLocation: (data) => dispatch({type: actionType.SUCCESS_FETCHING_LOCATION_DATA, payload: data}),

}
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);