import React, {Component} from 'react';
import moment from 'moment';
import './WeatherData.less';

class MainPage extends Component {


    render() {
        const { weather } = this.props;
        const ms = 1000;
        const date = weather.date*ms;
        const count = 2;
        const average = (weather.high+weather.low)/count;
        
        return (
            
            <div className="weather-block">
                <div className="temperature">
                    <p className="count">{average}°{localStorage.getItem('unit')}</p>
                    <b>{weather.text}</b>
                </div>
                <div className="date">
                    <b>{moment(date).format('dddd')}</b>
                    <p>{moment(date).format('D MMMM')}</p>
                </div>
            </div>
        );
    }
}


export default MainPage;