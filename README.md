Weather Web App
Build an app that does the following:

Retrieve the temperature and humidity at the user's current location (use browser geolocation API) from the Yahoo Weather API: https://developer.yahoo.com/weather/documentation.html#request

The application should retrieve this data when launched. It should also contain a refresh control to allow the user to update the data.

The application should store the last retrieved data in redux store and display it initially when launched. The screen should be refreshed when updated data is received and it should be refreshed immediately after launch.

Routes
`/` - forecast screen: this screen should show the temperature and humidity retrieved from Yahoo Weather API. It should also show the last time that data was received and refresh button to get new data manually.
`/settings` - this screen should have settings like Refresh Location and option to change default units (celsius or fahrenheit)
